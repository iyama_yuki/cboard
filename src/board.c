#include "stdbool.h"
#include "bit.h"
#include "board.h"
#include "flip_carry_64.c"
#include "move.h"

void board_swap_players(Board *board) {
  const unsigned long long tmp = board->player;
  board->player = board->opponent;
  board->opponent = tmp;
}

void board_init(Board *board) {
  board->player = 0x0000000810000000ULL;
  board->opponent = 0x0000001008000000ULL;
}

unsigned long long board_get_move(const Board *board, const int x, Move *move) {
  move->flipped = flip[x](board->player, board->opponent);
  move->x = x;
  return move->flipped;
}

bool board_check_move(const Board *board, Move *move) {
  if (move->x == PASS)
    return !can_move(board->player, board->opponent);
  else if ((x_to_bit(move->x) & ~(board->player | board->opponent)) == 0)
    return false;
  else if (move->flipped != flip[move->x](board->player, board->opponent))
    return false;
  else
    return true;
}

void board_update(Board *board, const Move *move) {
  board->player ^= (move->flipped | x_to_bit(move->x));
  board->opponent ^= move->flipped;
  board_swap_players(board);
}

unsigned long long get_some_moves(const unsigned long long P,
                                  const unsigned long long mask,
                                  const int dir) {
  unsigned long long flip;
  flip = (((P << dir) | (P >> dir)) & mask);
  flip |= (((flip << dir) | (flip >> dir)) & mask);
  flip |= (((flip << dir) | (flip >> dir)) & mask);
  flip |= (((flip << dir) | (flip >> dir)) & mask);
  flip |= (((flip << dir) | (flip >> dir)) & mask);
  flip |= (((flip << dir) | (flip >> dir)) & mask);
  return (flip << dir) | (flip >> dir);
}

unsigned long long get_moves(const unsigned long long P,
                             const unsigned long long O) {
  const unsigned long long mask = O & 0x7E7E7E7E7E7E7E7Eull;

  return (get_some_moves(P, mask, 1) | get_some_moves(P, O, 8) |
          get_some_moves(P, mask, 7) | get_some_moves(P, mask, 9)) &
         ~(P | O);
}

bool can_move(const unsigned long long P, const unsigned long long O) {
  const unsigned long long E = ~(P | O);

  return (get_some_moves(P, O & 0x007E7E7E7E7E7E00ull, 7) & E) ||
         (get_some_moves(P, O & 0x007E7E7E7E7E7E00ull, 9) & E) ||
         (get_some_moves(P, O & 0x7E7E7E7E7E7E7E7Eull, 1) & E) ||
         (get_some_moves(P, O & 0x00FFFFFFFFFFFF00ull, 8) & E);
}