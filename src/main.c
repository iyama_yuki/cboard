#include "ui.h"
#include <stdlib.h>

void option(char *str) {
  while (*str != '\0') {
    switch (*str++) {
    default:
      break;
    }
  }
}

int main(int argc, char *argv[]) {
  UI *ui;
  int i;
  for (i = 1; i < argc; ++i) {
    if (*argv[i] == '-') {
      option(argv[i] + 1);
    }
  }

  ui = (UI *)malloc(sizeof *ui);
  ui->init = ui_init_cboard;
  ui->loop = ui_loop_cboard;
  ui->free = ui_free_cboard;

  ui->init(ui);

  ui->loop(ui);
  ui->free(ui);

  free(ui);

  return 0;
}