#include "play.h"
#include "const.h"

void play_init(Play *play) {
  board_init(play->initial_board);

  play->player = play->initial_player = BLACK;

  play_new(play);
}

void play_new(Play *play) {
  *play->board = *play->initial_board;
  play->player = play->initial_player;
}

void play_update(Play *play, Move *move) {
  board_update(play->board, move);

  play->player ^= 1;
}

bool play_move(Play *play, int x) {
  Move move[1];

  *move = MOVE_INIT;
  board_get_move(play->board, x, move);
  if (board_check_move(play->board, move)) {
    play_update(play, move);
    return true;
  } else {
    return false;
  }
  return true;
}