#include "cboard.h"
#include "ui.h"
#include "play.h"

#include <ncurses.h>
#include <stdlib.h>
#include <unistd.h>

void ui_init_cboard(UI *ui) {
  Play *play = ui->play;
  play_init(play);

  initscr();
  start_color();
  init_pair(1, COLOR_WHITE, COLOR_BLACK);
  init_pair(2, COLOR_BLACK, COLOR_GREEN);
  init_pair(3, COLOR_WHITE, COLOR_GREEN);
  noecho();
  curs_set(0);
  keypad(stdscr, TRUE);
  mousemask(ALL_MOUSE_EVENTS, NULL);
  attron(A_BOLD);
  mvprintw(0, 0, "CBOARD\n");
  print_board(ui->play);
}

void ui_free_cboard(UI *ui) { endwin(); }

void ui_loop_cboard(UI *ui) {
  int x = NOMOVE;
  for (;;) {
    int ch = getch();
    if (ch == 'q') {
      return;
    }
    control_keyinput(&x, ch);
    if (x != NOMOVE) {
      if ((get_moves(ui->play->board->player, ui->play->board->opponent) &
           x_to_bit(x)) != 0) {
        if (play_move(ui->play, x)) {
          print_board(ui->play);
        }
      }
      x = NOMOVE;
    }
  }
}

void print_board(Play *play) {
  Board *board = play->board;
  const char player_color = play->player == BLACK ? '*' : 'o';
  const char opponent_color = play->player == BLACK ? 'o' : '*';
  unsigned long long m = get_moves(board->player, board->opponent);
  attron(COLOR_PAIR(2));
  mvprintw(2, 0, COLUMN_LABEL);
  mvprintw(3, 0, SEPARATOR_01);
  mvprintw(4, 0, "1");
  mvprintw(4, 1, SEPARATOR_03);
  mvprintw(5, 0, SEPARATOR_02);
  mvprintw(6, 0, "2");
  mvprintw(6, 1, SEPARATOR_03);
  mvprintw(7, 0, SEPARATOR_02);
  mvprintw(8, 0, "3");
  mvprintw(8, 1, SEPARATOR_03);
  mvprintw(9, 0, SEPARATOR_02);
  mvprintw(10, 0, "4");
  mvprintw(10, 1, SEPARATOR_03);
  mvprintw(11, 0, SEPARATOR_02);
  mvprintw(12, 0, "5");
  mvprintw(12, 1, SEPARATOR_03);
  mvprintw(13, 0, SEPARATOR_02);
  mvprintw(14, 0, "6");
  mvprintw(14, 1, SEPARATOR_03);
  mvprintw(15, 0, SEPARATOR_02);
  mvprintw(16, 0, "7");
  mvprintw(16, 1, SEPARATOR_03);
  mvprintw(17, 0, SEPARATOR_02);
  mvprintw(18, 0, "8");
  mvprintw(18, 1, SEPARATOR_03);
  mvprintw(19, 0, SEPARATOR_01);

  int i, j;
  for (i = 0; i < 8; i++) {
    for (j = 0; j < 8; j++) {
      if ((board->player >> (i * 8 + j)) & 1) {
        play->player == BLACK ? attron(COLOR_PAIR(2)) : attron(COLOR_PAIR(3));
        // attron(COLOR_PAIR(1));
        mvprintw(4 + i * 2, 3 + j * 4, "%c", player_color);
      } else if ((board->opponent >> (i * 8 + j)) & 1) {
        play->player == BLACK ? attron(COLOR_PAIR(3)) : attron(COLOR_PAIR(2));
        // attron(COLOR_PAIR(2));
        mvprintw(4 + i * 2, 3 + j * 4, "%c", opponent_color);
      } else if ((m >> (i * 8 + j)) & 1) {
        attron(COLOR_PAIR(2));
        mvprintw(4 + i * 2, 3 + j * 4, ".");
      } else {
      }
    }
  }
  attron(COLOR_PAIR(1));
  if (play->player == BLACK) {
    mvprintw(21, 0, "TURN : * (BLACK)\n", m);
  } else {
    mvprintw(21, 0, "TURN : o (WHITE)\n", m);
  }
  refresh();
}

void control_keyinput(int *x, int ch) {
  MEVENT e;

  switch (ch) {
  case KEY_UP:
    break;
  case KEY_DOWN:
    break;
  case KEY_RIGHT:
    break;
  case KEY_LEFT:
    break;
  case KEY_MOUSE:
    if (getmouse(&e) == OK) {
      detect_place(x, e.y, e.x);
    }
    break;
  }
}

void detect_place(int *x, int posy, int posx) {
  if (posy % 2 != 0)
    return;
  if (posy < 4 || posy > 18)
    return;
  if ((posx - 1) % 4 == 0)
    return;
  if (posx < 2 || posx > 32)
    return;
  *x = ((posy - 4) / 2) * 8 + ((posx - 1) / 4);
}