#ifndef BOARD_H
#define BOARD_H

#include "const.h"
#include "move.h"

typedef struct Board {
  unsigned long long player, opponent;
} Board;

void board_init(Board *board);
unsigned long long board_get_move(const Board *board, const int x, Move *move);
bool board_check_move(const Board *board, Move *move);
unsigned long long get_some_moves(const unsigned long long P,
                                  const unsigned long long mask, const int dir);
unsigned long long get_moves(const unsigned long long P,
                             const unsigned long long O);
bool can_move(const unsigned long long P, const unsigned long long O);

#endif