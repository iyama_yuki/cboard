#ifndef CBOARD_H
#define CBOARD_H

void option(char *str);
void print_board(Play *play);
void control_keyinput(int *x, int ch);
void detect_place(int *x, int posy, int posx);

#define COLUMN_LABEL "   A   B   C   D   E   F   G   H  \n"
#define SEPARATOR_01 "  --------------------------------\n"
#define SEPARATOR_02 " |---+---+---+---+---+---+---+---|\n"
#define SEPARATOR_03 "|   |   |   |   |   |   |   |   |\n"
#endif