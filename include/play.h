#ifndef PLAY_H
#define PLAY_H

#include "board.h"

typedef struct Play {
  Board board[1];
  Board initial_board[1];
  int player;
  int initial_player;
} Play;

void play_init(Play *play);
void play_new(Play *play);

#endif