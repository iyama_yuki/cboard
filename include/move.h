#ifndef MOVE_H
#define MOVE_H

typedef struct Move {
  unsigned long long flipped;
  int x;
  struct Move *next;
} Move;

extern const Move MOVE_INIT;

#endif