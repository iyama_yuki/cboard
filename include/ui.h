#ifndef UI_H
#define UI_H

typedef struct UI {
  Play play[2];

  void (*init)(struct UI *);
  void (*loop)(struct UI *);
  void (*free)(struct UI *);
} UI;

void ui_init_cboard(UI *ui);
void ui_loop_cboard(UI *ui);
void ui_free_cboard(UI *ui);

#endif