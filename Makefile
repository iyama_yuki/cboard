CC=clang

SRC=src/all.c
INCLUDE=include
TARGET=cboard
LDFLAGS=-lncurses

all: cboard

cboard: clean f
	$(CC) -o $(TARGET) $(SRC) -I $(INCLUDE) $(LDFLAGS)

f:
	clang-format -i src/* $(INCLUDE)/*

clean:
	rm -f $(TARGET)
